from st3m.application import Application, ApplicationContext
from st3m.ui.view import BaseView, ViewManager
from st3m.input import InputState
from ctx import Context
import st3m.run

class Batt(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        self._min_batt = 2.8
        self._max_batt = 4.2
        self._max_width = 200
        self._batt = -1.0

    def draw(self, ctx: Context) -> None:
        # Paint the background black
        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()
        
        width = (self._batt - self._min_batt) * self._max_width / (self._max_batt - self._min_batt)
        if (width < 0):
            width = 0
        
        if (width > self._max_width):
            width = self._max_width
        
        # Paint a red square in the middle of the display
        ctx.rgb(1.0, 1.0, 1.0).rectangle(-100, -21, self._max_width+2, 42).stroke()
        ctx.rgb(0, 0.8, 0.8).rectangle(-100, -20, width, 40).fill()
        
        if (self._batt >= 0):
            str = "Batt {:.02f}V".format(self._batt)
            w = ctx.text_width(str)
            ctx.move_to(-w/2,-50).text(str)
        
    
    def think(self, ins: InputState, delta_ms: int) -> None:
        self._batt = ins.battery_voltage

if __name__ == '__main__':
    # Continue to make runnable via mpremote run.
    st3m.run.run_view(Batt(ApplicationContext()))
